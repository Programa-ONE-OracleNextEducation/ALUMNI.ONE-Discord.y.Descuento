# **ALUMNI.ONE-Discord.y.Descuento**

## **Aquí puedes encontrar información sobre los beneficios de la Comunidad Alumni ONE.**

Alumni ONE es la comunidad de ex alumnos del Programa ONE - Oracle Next Education, en la que podrá establecer redes de contactos. Tendrás acceso a beneficios exclusivos, eventos y mucho más, para que puedas continuar tu desarrollo profesional y vital.

## **Paso a paso**

### **1. ¡Acceso al servidor exclusivo ALUMNI ONE en Discord!**

Aquí te dejamos la invitación para la comunidad exclusiva de Alumni ONE en [Discord](https://discord.gg/rt2VQGwN8k "Discord")!

### **2. Formaciones extra por 3 meses**

Las formaciones extra de My SQL, Data Science y OCI están disponibles para los Alumni ONE por 3 meses después del día de la graduación de su grupo.

- [SQL con MySQL Server de Oracle](https://app.aluracursos.com/formacion-oracle-mysql-one "SQL con MySQL Server de Oracle")

- [Python, Data Science en OCI y Oracle Analytics](https://app.aluracursos.com/formacion-data-science-oci-oracle-one "Python, Data Science en OCI y Oracle Analytics")

- [Oracle Cloud Infrastructure](https://app.aluracursos.com/formacion-oracle-cloud-infraestructure-one "Oracle Cloud Infrastructure")

### **3. ¡Descuento en los cursos Alura Latam!**

Todos los miembros de la **Comunidad ONE Alumni** tienen acceso a **20% de descuento** sobre el valor de los planes disponibles en Alura Latam, por un año después del día de la graduación de su grupo. _[Descuentos de Alura Latam: plan semestral y plan anual](https://www.aluracursos.com/promo/alumni-one "Descuentos de Alura Latam: plan semestral y plan anual")_

### **4. Comparte tu testimonio**

Queremos conocer tu historia de como el programa ONE transformo tu vida.

Animate a compartirnos en [este fomulário](https://docs.google.com/forms/d/1-mdlm2_-YWk4rfzEz2U0xl_8T3yShRUnQdO-ZwaQ12A/viewform?edit_requested=true)

### **5. Kit de Alumni**

¡Tenemos una sorpresa para ti!

El kit está compuesto por:

[Glosario](https://caelum-online-public.s3.amazonaws.com/oracle-one-fase2/one-lad-kit-finalizacion/%5BLAD%5D_Glosario_ONE%5B1%5D.pdf "Glosario"), en el que puedes consultar los principales términos del mundo tech.

[Plantilla](https://caelum-online-public.s3.amazonaws.com/oracle-one-fase2/Formandos+ES.pptx "Planilla") para la publicación en las redes sociales. Puedes utilizar esta plantilla para poner tu foto y publicarla en Linkedin, Facebook e Instagram para mostrar que formastes parte del Programa ONE. No olvides cambiar el número del gupo a que perteneces, utilizar el hashtag #oraclenexteducation y tagear @Oracle y @AluraLatam

Fondos de escritorio: Varios colores:

- [Azul](https://caelum-online-public.s3.amazonaws.com/oracle-one-fase2/one-lad-kit-finalizacion/BG_ZOOM_ONE_ESP_%281%29%5B1%5D.png "Azul")
- [Naranja](https://caelum-online-public.s3.amazonaws.com/oracle-one-fase2/one-lad-kit-finalizacion/BG_ZOOM_ONE_ESP_%282%29%5B1%5D.png "Naranja")
- [Verde](https://caelum-online-public.s3.amazonaws.com/oracle-one-fase2/one-lad-kit-finalizacion/BG_ZOOM_ONE_ESP_%283%29%5B1%5D.png "Verde")
- [Marron](https://caelum-online-public.s3.amazonaws.com/oracle-one-fase2/one-lad-kit-finalizacion/BG_ZOOM_ONE_ESP_%284%29%5B1%5D.png "Marron")

Una playlist colaborativa en Spotify "[Graduados en ONE](https://open.spotify.com/playlist/6RvZmNGIeLtkS6vLgTca9d?si=447302ff77064cd0&pt_success=1&nd=1 "Graduados en ONE")", ahi puedes compartir sus canciones favoritas y oir cosas nuevas :)
